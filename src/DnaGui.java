/**
 * DnaGui.java
 * Allows the user to select three codons and outputs the amino acid corresponding to the selections
 *
 * @author Mason Hicks
 */

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class DnaGui extends Application {

    private ToggleGroup[] atgc;
    private RadioButton[] a;
    private RadioButton[] t;
    private RadioButton[] g;
    private RadioButton[] c;
    private Button compute;
    private Label result;
    private final int COUNT_CODONS = 3;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.atgc = new ToggleGroup[COUNT_CODONS];
        this.a = new RadioButton[COUNT_CODONS];
        this.t = new RadioButton[COUNT_CODONS];
        this.g = new RadioButton[COUNT_CODONS];
        this.c = new RadioButton[COUNT_CODONS];
        this.compute = new Button("Compute");
        this.result = new Label();

        VBox[] columns = new VBox[COUNT_CODONS];

        for(int i=0; i<COUNT_CODONS; i++){
            this.atgc[i] = new ToggleGroup();
            this.a[i] = new RadioButton("A");
            this.t[i] = new RadioButton("T");
            this.g[i] = new RadioButton("G");
            this.c[i] = new RadioButton("C");

            this.a[i].setToggleGroup(this.atgc[i]);
            this.t[i].setToggleGroup(this.atgc[i]);
            this.g[i].setToggleGroup(this.atgc[i]);
            this.c[i].setToggleGroup(this.atgc[i]);

            columns[i] = new VBox(a[i],t[i],g[i],c[i]);
            columns[i].setSpacing(10);
        }

        HBox grid = new HBox(columns[0], columns[1], columns[2]);
        grid.setSpacing(10);
        grid.setPadding(new Insets(10));

        this.compute.setOnAction(new CalculateListener());
        VBox whole = new VBox(grid, this.compute, this.result);
        whole.setAlignment(Pos.CENTER);
        whole.setSpacing(10);

        Scene scene = new Scene(whole);
        primaryStage.setTitle("Codon Translator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    class CalculateListener implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            String codon = "";
            boolean missing = false;
            CodonTable table = new CodonTable();
            for(int i=0; i<COUNT_CODONS; i++) {
                if (a[i].isSelected()) codon += "A";
                else if (t[i].isSelected()) codon += "T";
                else if (g[i].isSelected()) codon += "G";
                else if (c[i].isSelected()) codon += "C";
                else missing = true;
            }
            if(missing) result.setText("");
            else result.setText(table.getAminoAcidSequence(codon));
        }
    }

}
